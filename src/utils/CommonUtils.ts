/* eslint-disable camelcase */

const CHAR_CODE_0: number = '0'.charCodeAt(0);
const CHAR_CODE_9: number = '9'.charCodeAt(0);
const CHAR_CODE_a: number = 'a'.charCodeAt(0);
const CHAR_CODE_z: number = 'z'.charCodeAt(0);
const CHAR_CODE_A: number = 'A'.charCodeAt(0);
const CHAR_CODE_Z: number = 'Z'.charCodeAt(0);

export function toKebabCase(str: string): string {
  const tokens: string[] = [];

  for (let index = 0; index < str.length; index++) {
    const char: string = str.charAt(index);
    const code: number = str.charCodeAt(index);

    if (code >= CHAR_CODE_A && code <= CHAR_CODE_Z) {
      if (index && tokens.at(-1) !== '-') {
        tokens.push('-');
      }

      tokens.push(String.fromCharCode(code + 32));
    } else if ((code >= CHAR_CODE_a && code <= CHAR_CODE_z) || (code >= CHAR_CODE_0 && code <= CHAR_CODE_9)) {
      tokens.push(char);
    } else {
      tokens.push('-');
    }
  }

  return tokens.join('');
}

export function clear(object: object): void {
  for (const key in object) {
    delete object[key];
  }
}

export function get(object: object, path: string): any {
  const paths: string[] = parsePath(path);

  return paths.length
    ? foundPaths(object, paths, null, (result: boolean, o: object, key: string) => (result ? o[key] : undefined))
    : object;
}

function parsePath(path: string): string[] {
  return path.split(/\./).filter((item: string) => item);
}

function foundPaths(object: object, [key, ...nextKeys]: string[], value: any, callback: Function, initMode = false): any {
  if (typeof object === 'object' && object !== null) {
    const nextKey: string = nextKeys[0];

    if (initMode && !(key in object)) {
      object[key] = {};
    }
    if (nextKeys.length === 1 && nextKey) {
      const entry: any = object[key];
      const entryIsArray: boolean = entry instanceof Array;
      const entries: any[] = entryIsArray ? entry : [entry];
      const results: any[] = value instanceof Array
        ? entries.filter((item: any) => item).map((item: any, index: number) => callback(true, item, nextKey, value[index]))
        : entries.filter((item: any) => item).map((item: any) => callback(true, item, nextKey, value));

      return entryIsArray ? results : results[0];
    }
    if (key in object) {
      return nextKey
        ? foundPaths(object[key], nextKeys, value, callback, initMode)
        : callback(true, object, key, value);
    }
    if (object instanceof Array) {
      return object.map((item: any) => foundPaths(item, nextKeys, value, callback, initMode));
    }
  }

  return callback(false, object, key);
}

export function copy(object: object): object {
  if (typeof object !== 'object' || object === null) {
    return object;
  }
  if (object instanceof Array) {
    return object.map(copy);
  }

  const copyObject: object = {};

  for (const key in object) {
    const value: any = object[key];

    if (typeof value === 'object' && value !== null) {
      if (value instanceof Array) {
        copyObject[key] = value.map((item: any) => copy(item));
      } else {
        copyObject[key] = copy(value);
      }
    } else {
      copyObject[key] = value;
    }
  }

  return copyObject;
}

export function merge(dest: object, src: object, strategy = 'append'): object {
  const isObject: Function = (obj: any): boolean => obj && typeof obj === 'object';

  if (isObject(dest) && isObject(src)) {
    for (const key in src) {
      if (isObject(src[key])) {
        dest[key] = key in dest && isObject(dest[key])
          ? merge(dest[key], src[key], strategy)
          : copy(src[key]);
      } else if (dest[key] instanceof Array) {
        if (strategy === 'replace') {
          dest[key].splice(0);
        }

        // dest[key].push(...[copy(src[key])])
        dest[key].push(...copy(src[key]) as any[]);
      } else {
        Object.assign(dest, { [key]: src[key] });
      }
    }
  } else if (src instanceof Array && dest instanceof Array) {
    dest.push(...copy(src) as any[]);
  }

  return dest;
}
